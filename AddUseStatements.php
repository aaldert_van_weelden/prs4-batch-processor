<?php

class AddUseStatements
{
    
    const SRC_PATH = 'elw-8/';
    
    const PROBE = "app";
    
    const CLASSPATHS = 'reports/classpaths.txt';
    
    /**
     * 
     * @var string
     */
    const DS = "\\";
    
    const SEMIC = ';';
    
    const ARROW = '->';
    
    /**
     * Modify for Mac to \n
     * @var string
     */
    const NLBR = "\r\n";

    private $logfile;
    
    /**
     *
     * @var array
     */
    public array $classFiles;
    
    /**
     * 
     * @var array
     */
    public array $classPaths;
    
    /**
     * 
     * @var array
     */
    public array $useStatements;
    
    /**
     *
     * @var array
     */
    public array $aliasStatements;
    
    /**
     * 
     */
    public function __construct()
    {
        $this->classFiles = [];
        $this->classPaths = [];
        $this->useStatements = [];
        $this->aliasStatements = [];
        $this->logfile = fopen('reports/add_usestatements.report.txt', "w");
    }
    
    /**
     * 
     * @param string $dir
     * @param array $results
     * @return array
     */
    private function getDirContents(string $dir, array &$results = [])
    {
        $files = scandir($dir);
        
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }
        
        return $results;
    }
    
    /**
     * 
     * @return self
     */
    public function getClassFiles()
    {
        $paths = $this->getDirContents(self::SRC_PATH);
    
        foreach ($paths as $path)
        { 
            $parts = explode(DIRECTORY_SEPARATOR, $path);
            $classFile = array_pop($parts);

            $className  = ucfirst(str_replace('.php', '', $classFile));

            if(is_file($path) && strpos($path, '.php') !== false) $this->classFiles[$path] = $className;  
        }        
        //var_dump($this->classFiles);die;
        return $this;
    }
    
    /**
     * 
     * @return self
     */
    public function getStatements(): self
    {
        $lines = file(self::CLASSPATHS);
        
        //var_dump($lines);
        
        foreach ($lines as $line) 
        {
            $parts = explode(self::ARROW, $line);
            $this->classPaths[$parts[0]] = $parts[1]; 
        }
        return $this;
    }
    
    /**
     * 
     * @return self
     */
    public function addStatements()
    {
        
        foreach ($this->classPaths as $classFile => $classPath)
        {
            print $classPath;
            
            $classPath = trim($classPath);
            
            foreach ($this->classFiles as $file => $className)
            {  
                if($this->sameDir($classFile, $file)) continue;

                $parts = explode(self::DS, $classPath); 
                $needleClassName = array_pop($parts);
                 
                $lines = file($file);
                
                $statementIsFound = false;
                $classIsUsed = false;
                $namespaceStatementLineNumber = 0;
                $lastUseStatementLinenumber = 0;
                
                foreach ($lines as $index => &$line)
                {
                   //insertion pointers
                   if($this->lineIsUseStatement($line)) $lastUseStatementLinenumber = $index;
                   if($this->lineIsAliasStatement($line)) $this->aliasStatements[$classPath.'_'.$index] = $file.' : '.$line;
                   if($this->lineIsNamespaceStatement($line)) $namespaceStatementLineNumber = $index;
                   
                   //check if the classname is used as a class reference in the current line
                   if( $this->classIsUsed($needleClassName, $line) )
                   {
                       $classIsUsed = true;
                       //check if the current line is a use statement
                       if($this->lineIsUseStatement($line))
                       {
                           //replace existing usestatement declaration
                           $newStatement = 'use '.$classPath.self::SEMIC.self::NLBR;
                           
                           if(strcmp( trim($newStatement), trim($line)) !== 0 ) $this->reportNewUsestatement($newStatement, $file, $index, 'replaced by: ');
                               
                           $line = $newStatement;
                           
                           $statementIsFound = true;     
                       }
                       else 
                       {
                           //class instance found
                           //remove global reference
                           if( $this->classIsGlobal($needleClassName, $line)) $line = str_replace('\\'.$needleClassName, $needleClassName, $line);  
                       }
                   }
                }
                
                if(! $statementIsFound && $classIsUsed)
                { 
                   //no existing use statement found
                   $line2insert = $lastUseStatementLinenumber + 1;
                   
                   $line2add = 'use '.$classPath.self::SEMIC.self::NLBR;
                   
                   if($lastUseStatementLinenumber == 0) $line2insert = $namespaceStatementLineNumber + 1;
                
                   array_splice($lines, $line2insert, 0, $line2add);
                
                   $this->reportNewUsestatement($line2add, $file, $line2insert, 'added stmnt: ');
                }
                
                file_put_contents($file, $lines);
            }   
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $line
     * @return bool
     */
    private function lineIsUseStatement($line): bool
    {
        return substr_compare($line, "use ", 0, 4) === 0;
    }
    
    /**
     * @param string $line
     * @return bool
     */
    private function lineIsNamespaceStatement($line): bool
    {
        return substr_compare($line, "namespace ", 0, 10) === 0;
    }
    
    /**
     * 
     * @param string $line
     * @return bool
     */
    private function lineIsAliasStatement($line): bool
    {
        if( ! $this->lineIsUseStatement($line)) return false;
        
        $haystack = explode(' ', $line);
        
        if(in_array('as', $haystack)) return  true;
        
        return false;
    }
    
    /**
     * 
     * @param string $needle
     * @param string $haystack
     * @return bool
     */
    private function classIsUsed($needle, $haystack): bool
    {
        $pattern = "/\b".trim($needle)."\b[\s]{0,2}[(|:|;]/";
       
        return filter_var(preg_match($pattern, $haystack), FILTER_VALIDATE_BOOLEAN );
    }
    
    /**
     *
     * @param string $needle
     * @param string $haystack
     * @return bool
     */
    private function classIsGlobal($needle, $haystack): bool
    {
        $pattern = "/[\\]\b".trim($needle)."\b[\s]{0,2}[(|:|;]/";
        
        return filter_var(preg_match($pattern, $haystack), FILTER_VALIDATE_BOOLEAN );
    }
    /**
     * 
     * @param string $file1
     * @param string $file2
     * @return bool
     */
    private function sameDir($file1, $file2): bool
    { 
       return  strcasecmp(dirname($file1), dirname($file2)) === 0;
    }
    
    private function reportNewUsestatement($newStatement, $file, $linenumber, $mode)
    { 
        $parts = explode(DIRECTORY_SEPARATOR, $file);
        $classFile = array_pop($parts);
        $className  = str_replace('.php', '', $classFile);
        
        $this->useStatements[$className] = rtrim(ltrim(trim($newStatement), 'use '), self::SEMIC).self::DS.$className;
        
        $classPath = implode(DIRECTORY_SEPARATOR, $parts).DIRECTORY_SEPARATOR.$className;

        $message = $mode.' '.trim($newStatement).' for class '.$classPath.' at line '.$linenumber.self::NLBR;
        fputs($this->logfile, $message);
    }
    
    /**
     * 
     * @return string
     */
    public function report() : string
    {
        $report = [];
        foreach ($this->classFiles as $file => $namespace) {
            $report[] = str_pad($file, 130).' -> '.$namespace;
        }
        $data =  implode("\r\n", $report);
        
        file_put_contents('reports/classfiles.txt', $data);
        
        file_put_contents('reports/usestatements.txt', implode("\r\n", $this->useStatements));
        
        $this->aliasStatements = array_unique($this->aliasStatements);
        
        file_put_contents('reports/aliasstatements.reports.txt', implode("\r\n", $this->aliasStatements));
        
        return $data;
    }
    
    /**
     * 
     */
    public function __destruct()
    {
        fclose($this->logfile);
    }
}



$statementAdder = new AddUseStatements();

$statementAdder
->getClassFiles()
->getStatements()
->addStatements()
->report();

<?php

class PSR4Namespacer
{
    
    const SRC_PATH = 'elw-8/';
    
    const PROBE = "app";
    
    /**
     * 
     * @var string
     */
    const DS = "\\";
    
    const SEMIC = ';';
    
    /**
     * Modify for Mac to \n
     * @var string
     */
    const NLBR = "\r\n";
    
    const ARROW = '->';
    
    /**
     * 
     * @var array
     */
    public array $namespaces;
    
    private $logfile;
    
    /**
     * 
     * @var array
     */
    public array $classPaths;
    
    /**
     * 
     */
    public function __construct()
    {
        $this->namespaces = [];
        $this->classPaths = [];
        $this->logfile = fopen('reports/newspaces.report.txt', "w");
    }
    
    /**
     * 
     * @param string $dir
     * @param array $results
     * @return array
     */
    private function getDirContents(string $dir, array &$results = [])
    {
        $files = scandir($dir);
        
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }
        
        return $results;
    }
    
    /**
     * 
     * @return PSR4Namespacer
     */
    public function get_PSR4_namespaces()
    {
        $paths = $this->getDirContents(self::SRC_PATH);
    
        foreach ($paths as $path)
        {
            $relativePath = ltrim(explode(self::PROBE, $path)[1],DIRECTORY_SEPARATOR);
 
            $parts = explode(DIRECTORY_SEPARATOR, $relativePath);
            array_pop($parts);
            
            
            foreach ($parts as &$part) 
            {
                $part = ucfirst($part);
            }
            
            $namespace = ucfirst(self::PROBE).self::DS.implode(self::DS, $parts);

            if(is_file($path) && strpos($path, '.php') !== false) $this->namespaces[$path] = $namespace;
            
            if(is_dir($path))
            { 
                $parts = explode(DIRECTORY_SEPARATOR, $path);
                $dir = array_pop($parts);
                
                if($dir !== self::PROBE) 
                {
                    $parts[] = ucfirst($dir);
                    $newname = implode(DIRECTORY_SEPARATOR, $parts);
                    rename($path, $newname);
                }
            }
        }
        
        //var_dump($this->namespaces);die;
        
        return $this;
    }
    
    /**
     * 
     */
    public function addNamespaces()
    {
        foreach ($this->namespaces as $file => $namespace)
        { 
           print $file.self::NLBR;
           
           $lines = file($file);
           
           $isNamespaced = false;
           
           $index = 0;
           
           foreach ($lines as &$line)
           {
               if(strpos($line, 'namespace ') !==  false)
               {
                   if($index == 0)
                   {
                       //namespace declaration is on first line after php tag
                       $line = "<?php".self::NLBR;
                       $isNamespaced = false;
                       break;
                   }
                   //replace existing namespace declaration
                   $newspace = 'namespace '.$namespace.self::SEMIC.self::NLBR;
                   
                   if(strcmp( trim($newspace), trim($line)) !== 0 ) $this->reportNewNamespace($newspace, $file, 'replaced');
                       
                   $line = $newspace;
                   
                   $isNamespaced = true;
                   break;
               }
               $index++;
           }

           if(! $isNamespaced)
           {
               //remove first line with php tag
               unset($lines[0]);
               
               //add php tag on the first line and add namespace declaration on the third line (Laravel convention)
               $line2add = 'namespace '.$namespace.self::SEMIC;
               
               $this->reportNewNamespace($line2add, $file, 'added');
               
               array_unshift($lines, "<?php".self::NLBR, self::NLBR, $line2add, self::NLBR);
           }

           file_put_contents($file, $lines);
        }
    }
    
    private function reportNewNamespace($newSpace, $file, $mode)
    { 
        $parts = explode(DIRECTORY_SEPARATOR, $file);
        $classFile = array_pop($parts);
        $className  = str_replace('.php', '', $classFile);
        
        $this->classPaths[$file] = rtrim(ltrim(trim($newSpace), 'namespace '), self::SEMIC).self::DS.$className;

        $message = $mode.' '.trim($newSpace).' for class '.$className.self::NLBR;
        fputs($this->logfile, $message);
    }
    
    /**
     * 
     * @return string
     */
    public function __toString() : string
    {
        $report = [];
        foreach ($this->namespaces as $file => $namespace) {
            $report[] = str_pad($file, 130).' -> '.$namespace;
        }
        $namepacedata =  implode("\r\n", $report);
        
        file_put_contents('reports/namespaces.txt', $namepacedata);
        
        $report = [];
        foreach ($this->classPaths as $file => $classpath) {
            $report[] = $file.'->'.$classpath;
        }
        $classpathdata =  implode("\r\n", $report);
        
        file_put_contents('reports/classpaths.txt', $classpathdata);
        
        return $namepacedata;
    }
    
    /**
     * 
     */
    public function __destruct()
    {
        fclose($this->logfile);
    }
}



$nameSpacer = new PSR4Namespacer();

$nameSpacer->get_PSR4_namespaces();

$nameSpacer->addNamespaces();

print($nameSpacer);


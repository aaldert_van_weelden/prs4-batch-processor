## About PSR4 batch processor

Put the PSR4Namespacer.php script outside the project root, specify the directory to probe (default is app) and run the script.
This script will 
- add or edit the namespace of the class files
- change lowercase classfile names to ucfirst
- change lowercase directory names into ucfirst

Put the AddUseStatements.php script in the same directory as the PSR4Namespacer.php script

### How to process files in the psr_4 project

- copy the directories and files to process (eg. services, models, controllers) into the psr4_batch/elw-8/app directory using the Laravel 8 directory structure
- run `php PSR4Namespacer.php` This will either change the existing or add the correct namespaces and also generate some sourcefiles and diagnostic files. Next:
- run `php AddUseStatements.php`  This script processes the generated `classpaths.txt` source file. This will either change the existing use statements or add the missing ones. Also some diagnostic files are generated

Default Windows line endings are used, this can be changed for now by editing the `NLBR` constant in the scripts